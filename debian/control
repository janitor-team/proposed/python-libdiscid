Source: python-libdiscid
Section: python
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders: Sebastian Ramacher <sramacher@debian.org>
Build-Depends:
 debhelper-compat (= 12),
 dh-python,
 libdiscid-dev (>= 0.6),
 python3-all-dev,
 python3-all-dbg,
 python3-setuptools,
 python3-pkgconfig,
 cython3,
 cython3-dbg,
Build-Depends-Indep:
 python3-sphinx,
 python3-doc
Standards-Version: 4.4.1
Vcs-Git: https://salsa.debian.org/multimedia-team/python-libdiscid.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/python-libdiscid
Homepage: https://pypi.python.org/pypi/python-libdiscid
Rules-Requires-Root: no

Package: python3-libdiscid
Architecture: any
Depends:
 ${python3:Depends},
 ${shlibs:Depends},
 ${misc:Depends}
Suggests:
 python-libdiscid-doc
Description: libdiscid binding for Python 3
 libdiscid allows one to create MusicBrainz DiscIDs from audio CDs. It reads a
 CD's table of contents and generates and identifier which can be used to
 lookup the CD at MusicBrainz. python-libdiscid provides a binding to work with
 libdiscid from Python.
 .
 This package provides the binding for Python 3.

Package: python3-libdiscid-dbg
Section: debug
Architecture: any
Depends:
 python3-libdiscid (= ${binary:Version}),
 ${shlibs:Depends},
 ${misc:Depends},
 ${python3:Depends}
Description: libdiscid binding for Python 3 (debug extension)
 libdiscid allows one to create MusicBrainz DiscIDs from audio CDs. It reads a
 CD's table of contents and generates and identifier which can be used to
 lookup the CD at MusicBrainz. python-libdiscid provides a binding to work with
 libdiscid from Python.
 .
 This package provides the extensions built for the Python 3 debug interpreter.

Package: python-libdiscid-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends}
Built-Using:
 ${sphinxdoc:Built-Using}
Description: libdiscid binding for Python (documentation)
 libdiscid allows one to create MusicBrainz DiscIDs from audio CDs. It reads a
 CD's table of contents and generates and identifier which can be used to
 lookup the CD at MusicBrainz. python-libdiscid provides a binding to work with
 libdiscid from Python.
 .
 This package contains the documentation.
